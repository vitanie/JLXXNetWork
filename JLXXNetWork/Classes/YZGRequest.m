//
//  YZGRequest.m
//  sisitv_ios
//
//  Created by apple on 16/12/6.
//  Copyright © 2016年 JLXX--YZG. All rights reserved.
//

#import "YZGRequest.h"
#import "YZGRequestManager.h"
#import <objc/runtime.h>

NSString *const YZGRequestValidationErrorDomain = @"com.deerlive.request.validation";

@implementation YZGRequest


-(void)cheeckRequestParams{
	
	unsigned int count = 0;
	Ivar *ivarList = class_copyIvarList(self.class, &count);
	for (int i = 0 ; i < count; i++) {
		// 获取成员属性
		Ivar ivar = ivarList[i];
		
		// 成员属性类型
		NSString *propertyType = [NSString stringWithUTF8String:ivar_getTypeEncoding(ivar)];
		//是否是字符串
		BOOL isString  = [propertyType containsString:@"NSString"];
		if (!isString) {
			continue;
		}
		//是否为nil
		id ivarValue = object_getIvar(self, ivar);
		if (!ivarValue) {
			object_setIvar(self, ivar, @"");
		}
	}
}

-(instancetype)init{
	if (self = [super init]) {
		self.requestMethod = YZGRequestMethodPOST;
	}
	return self;
}

-(instancetype)initWithRequestUrl:(NSString *)requestUrl{
	if (self = [super init]) {
		self.requestUrl = requestUrl;
		self.requestMethod = YZGRequestMethodPOST;
	}
	return self;
}

-(instancetype)initWithRequestParam:(id)requestParam{
	if (self = [super init]) {
		self.requestParam = requestParam;
		self.requestMethod = YZGRequestMethodPOST;
	}
	return self;
}

-(instancetype)initWithRequestUrl:(NSString *)requestUrl withRequestParam:(nullable id)requestParam{
	if (self = [super init]) {
		self.requestUrl = requestUrl;
		self.requestParam = requestParam;
		self.requestMethod = YZGRequestMethodPOST;
	}
	return self;
}


#pragma mark - Request Configuration

- (YZGRequestSerializerType)requestSerializerType {
	return YZGRequestSerializerTypeHTTP;
}

-(NSArray<NSString *> *)ignoreParams{
	return nil;
}

- (NSArray *)requestAuthorizationHeaderFieldArray {
	return nil;
}

- (NSURLRequest *)currentRequest {
	return self.requestTask.currentRequest;
}

- (NSURLRequest *)originalRequest {
	return self.requestTask.originalRequest;
}

- (BOOL)isCancelled {
	if (!self.requestTask) {
		return NO;
	}
	return self.requestTask.state == NSURLSessionTaskStateCanceling;
}

- (BOOL)isExecuting {
	if (!self.requestTask) {
		return NO;
	}
	return self.requestTask.state == NSURLSessionTaskStateRunning;
}

#pragma mark - Response Information

- (YZGResponseSerializerType)responseSerializerType {
	return YZGResponseSerializerTypeJSON;
}

-(NSSet<NSString *> *)acceptableContentTypes{
	return [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html" ,@"text/plain",@"audio/mpeg",nil];
}

- (NSHTTPURLResponse *)response {
	return (NSHTTPURLResponse *)self.requestTask.response;
}

- (NSInteger)responseStatusCode {
	return [self.responseObject[@"code"] integerValue];
}
-(NSDictionary *)responseHeaders{
	return self.response.allHeaderFields;
}

- (BOOL)statusCodeValidator {
	NSInteger statusCode = [self responseStatusCode];
	return (statusCode >= 200 && statusCode <= 299);
}

-(BOOL)jsonValidator{
	return YES;
}

#pragma mark - Request Action

- (void)start {
	[[YZGRequestManager sharedInstance] addRequest:self];
}
- (void)startWithCompletionBlockWithSuccess:(YZGRequestCompletionBlock)success
									failure:(YZGRequestCompletionBlock)failure {
	[self setCompletionBlockWithSuccess:success failure:failure];
	[self start];
}

- (void)setCompletionBlockWithSuccess:(YZGRequestCompletionBlock)success
							  failure:(YZGRequestCompletionBlock)failure {
	self.successCompletionBlock = success;
	self.failureCompletionBlock = failure;
}

- (void)stop {
	[[YZGRequestManager sharedInstance] cancelRequest:self];
}

- (void)clearCompletionBlock {
	// nil out to break the retain cycle.
	self.successCompletionBlock = nil;
	self.failureCompletionBlock = nil;
	self.constructingBodyBlock = nil;
	self.resumableDownloadProgressBlock = nil;
	self.uploadProgressBlock = nil;
}

- (NSTimeInterval)requestTimeoutInterval {
	return 20;
}

- (BOOL)allowsCellularAccess {
	return YES;
}

-(void)serializedResponseObjectToModel{
	//子类实现
}

- (NSString *)description {
	return [NSString stringWithFormat:@"<%@: %p>{ URL: %@ } { method: %@ } { arguments: %@ } {status code = %ld} ,{error = %@}", NSStringFromClass([self class]), self, self.currentRequest.URL, self.currentRequest.HTTPMethod, self.requestParam,self.responseStatusCode,self.error.localizedDescription];
}

-(void)dealloc{
#ifdef DEBUG
	NSLog(@"requestName: %@ dealloc",NSStringFromClass([self class]));
#else
#endif
	
}

@end
