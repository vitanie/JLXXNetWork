//
//  YZGRequestConfig.m
//  Pods
//
//  Created by apple on 17/5/12.
//
//

#import "YZGRequestConfig.h"

#if __has_include(<AFNetworking/AFNetworking.h>)
#import <AFNetworking/AFNetworking.h>
#else
#import "AFNetworking.h"
#endif

NSString * const YZGNetworkingReachabilityDidChangeNotification = @"com.deerlive.networking.reachability.change";
NSString * const YZGNetworkingReachabilityNotificationStatusItem = @"YZGNetworkingReachabilityNotificationStatusItem";

@implementation YZGRequestConfig{
    NSMutableDictionary *_defaultParam;
    dispatch_queue_t _processingQueue;
}

+ (instancetype)sharedInstance{
    static id sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _baseURL = @"http://v4.deerlive.com";
        _securityPolicy = [AFSecurityPolicy defaultPolicy];
        _networkStatus = YZGNetworkReachabilityStatusUnknown;
        _processingQueue = dispatch_queue_create("com.deerlive.networkRequestManager.yzgprocess", DISPATCH_QUEUE_CONCURRENT);
        _defaultParam = [self params];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:AFNetworkingReachabilityDidChangeNotification object:nil];
        [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    }
    return self;
}

- (void)reachabilityChanged:(NSNotification *)notification
{
    YZGNetworkReachabilityStatus status = [notification.userInfo[AFNetworkingReachabilityNotificationStatusItem] integerValue];
    self.networkStatus = status;
    NSDictionary *userInfo = @{YZGNetworkingReachabilityNotificationStatusItem:@(status)};
    [[NSNotificationCenter defaultCenter] postNotificationName:YZGNetworkingReachabilityDidChangeNotification object:nil userInfo:userInfo];
}

-(NSDictionary *)defaultParam{
    return [_defaultParam copy];
}

-(void)appendDefaultParam:(NSDictionary *)param{
    [_defaultParam setValuesForKeysWithDictionary:param];
}
-(void)removeParamFor:(NSString *)key{
	[_defaultParam removeObjectForKey:key];
}
-(NSMutableDictionary *)params{
    NSMutableDictionary * defaultParam = [NSMutableDictionary dictionary];
    [defaultParam setObject:@"ios" forKey:@"os"];
    [defaultParam setObject:[[UIDevice currentDevice] systemVersion] forKey:@"os_ver"];
    [defaultParam setObject:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"] forKey:@"soft_ver"];
    return defaultParam;
}

@end
