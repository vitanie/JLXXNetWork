Pod::Spec.new do |s|
  s.name             = 'JLXXNetWork'
  s.version          = '0.3.2'
  s.summary          = 'JLXXNetWork.'
  s.description      = '网络请求类,改编自YTKNetWork'

  s.homepage         = 'https://gitee.com/cnsuer/JLXXNetWork'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'cnsuer' => '842393459@qq.com' }
  s.source           = { :git => 'https://gitee.com/cnsuer/JLXXNetWork.git', :tag => s.version.to_s }


  s.ios.deployment_target = '8.0'
  s.libraries = 'System'

  s.source_files = 'JLXXNetWork/Classes/**/*'
  s.dependency 'AFNetworking','~> 3.1.0'
end
