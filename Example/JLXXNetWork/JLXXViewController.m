//
//  JLXXViewController.m
//  JLXXNetWork
//
//  Created by cnsuer on 07/17/2017.
//  Copyright (c) 2017 cnsuer. All rights reserved.
//

#import "JLXXViewController.h"
#import "JLXXMineUploadProduction.h"
#import "JLXXGetAppConfig.h"


@interface JLXXViewController ()

@end

@implementation JLXXViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a
    
    
        JLXXGetAppConfig *upload = [[JLXXGetAppConfig alloc] init];
        [upload startWithCompletionBlockWithSuccess:^(__kindof YZGRequest * _Nonnull request) {
            NSLog(@"%@",request.responseObject);
        } failure:^(__kindof YZGRequest * _Nonnull request) {
            NSLog(@"%@",request.error.localizedDescription);
        }];
    
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UIImage *imge = [UIImage imageNamed:@"底部我的选中"];
    JLXXMineUploadProduction *upload = [[JLXXMineUploadProduction alloc] initWithType:@"1" description:@"草泥马" location:@"日本" image:@[imge]];
    [upload startWithCompletionBlockWithSuccess:^(__kindof YZGRequest * _Nonnull request) {
        NSLog(@"%@",request.responseObject);
    } failure:^(__kindof YZGRequest * _Nonnull request) {
        NSLog(@"%@",request.error.localizedDescription);
    }];
    [upload setUploadProgressBlock:^(NSProgress *pro){
        NSLog(@"上传图片进度:---%f----",pro.fractionCompleted);
    }];
    
}

@end
