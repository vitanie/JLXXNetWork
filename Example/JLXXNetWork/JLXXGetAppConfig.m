//
//  JLXXGetAppConfig.m
//  DeerLive
//
//  Created by apple on 2017/6/29.
//  Copyright © 2017年 JLXX--YZG. All rights reserved.
//

#import "JLXXGetAppConfig.h"

@implementation JLXXGetAppConfig

-(NSString *)requestUrl{
    return @"/Api/Appconfig/getAPPConfig";
}

@end
