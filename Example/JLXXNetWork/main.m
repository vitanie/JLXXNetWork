//
//  main.m
//  JLXXNetWork
//
//  Created by cnsuer on 07/17/2017.
//  Copyright (c) 2017 cnsuer. All rights reserved.
//

@import UIKit;
#import "JLXXAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([JLXXAppDelegate class]));
    }
}
