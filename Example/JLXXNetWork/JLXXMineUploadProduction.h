//
//  JLXXMineUploadProduction.h
//  sisitv_ios
//
//  Created by apple on 17/5/18.
//  Copyright © 2017年 JLXX--YZG. All rights reserved.
//

#import <JLXXNetWork/JLXXNetWork.h>

@interface JLXXMineUploadProduction : YZGRequest

- (id)initWithType:(NSString *)type description:(NSString *)description location:(NSString *)location image:(NSArray *)imageArray;

@end
