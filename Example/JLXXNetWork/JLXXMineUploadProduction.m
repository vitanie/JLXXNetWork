//
//  JLXXMineUploadProduction.m
//  sisitv_ios
//
//  Created by apple on 17/5/18.
//  Copyright © 2017年 JLXX--YZG. All rights reserved.
//

#import "JLXXMineUploadProduction.h"
#import "AFURLRequestSerialization.h"
@implementation JLXXMineUploadProduction{
    //作品类型:1照片，2视频
    NSString *_type;
    //描述：
    NSString *_description;
    //位置:
    NSString *_location;
    //image
    NSArray *_imageArray;
}

- (id)initWithType:(NSString *)type description:(NSString *)description location:(NSString *)location image:(NSArray *)imageArray{
    self = [super init];
    if (self) {
        _type = type;
        _description = description;
        _location = location;
        _imageArray = imageArray;
    }
    return self;
}

-(NSString *)requestUrl{
    return @"/Api/SiSi/userUploadWorks";
}

-(AFConstructingBlock)constructingBodyBlock{
    return ^(id<AFMultipartFormData> formData) {
        NSString *name = @"image.png";
        NSString *formKey = @"smeta[]";
        NSString *type = @"image/png";
        for(NSInteger i = 0; i < _imageArray.count; i++)
        {
            UIImage *image = [_imageArray objectAtIndex: i];
            NSData *data = UIImagePNGRepresentation(image);
            [formData appendPartWithFileData:data name:formKey fileName:name mimeType:type];
            NSLog(@"正在上传第----%lu张-------",i);
        }
    };
}

-(id)requestParam{
    if (!_type) return nil;
    if (!_description) _description = @"";
    if (!_location) _location = @"";
    NSDictionary *param = @{@"type":_type,@"location":_location,@"description":_description};
    return param;
}

-(BOOL)jsonValidator{
    if ([self.responseObject isKindOfClass:[NSDictionary class]]) {
        return YES;
    }
    return YES;
}



@end
