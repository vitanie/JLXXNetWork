//
//  JLXXAppDelegate.h
//  JLXXNetWork
//
//  Created by cnsuer on 07/17/2017.
//  Copyright (c) 2017 cnsuer. All rights reserved.
//

@import UIKit;

@interface JLXXAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
