#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "AFJSONResponseSerializer+AES.h"
#import "JLXXNetWork.h"
#import "NSData+JLXXAES.h"
#import "NSObject+JLXXNetWork.h"
#import "YZGBatchRequest.h"
#import "YZGChainRequest.h"
#import "YZGRequest.h"
#import "YZGRequestConfig.h"
#import "YZGRequestManager.h"

FOUNDATION_EXPORT double JLXXNetWorkVersionNumber;
FOUNDATION_EXPORT const unsigned char JLXXNetWorkVersionString[];

